import type { AWS } from "@serverless/typescript";

import hello from "@functions/hello";
import signup from "@functions/signup";
import auth from "@functions/auth";
import dbmigration from "@functions/dbmigration";
import saveanswer from "@functions/saveanswer";
import getanswers from "@functions/getanswers";
import urlLogin from "@functions/url-login";
import login from "@functions/login";
import changePassword from "@functions/change-password";
import requestChangePassword from "@functions/change-password-request";
import changeUsername from "@functions/change-username";

const serverlessConfiguration: AWS = {
  service: "dfg-backend",
  frameworkVersion: "3",
  configValidationMode: "error",
  plugins: [
    "serverless-esbuild",
    "serverless-sequelize-migrations",
    "serverless-offline",
  ],
  provider: {
    name: "aws",
    runtime: "nodejs14.x",
    apiGateway: {
      minimumCompressionSize: 1024,
      shouldStartNameWithService: true,
    },
    environment: {
      AWS_NODEJS_CONNECTION_REUSE_ENABLED: "1",
      NODE_OPTIONS: "--enable-source-maps --stack-trace-limit=1000",
      AUTH0_DOMAIN: "dev-eyhevqlj.us.auth0.com",
      AUTH0_CLIENT_ID: "XRcs96d1QS74PIwjrxgDDuHvCvLXYL2e",
      FRONTEND_URL: "https://app.stg.dfg-launch.com",
      AUTH0_REALM: "Username-Password-Authentication",
      CREDENTIALS_ENCRYPTION_KEY: "dfg_secret",
      DB_CONNECTION_URL:
        "postgres://${self:custom.dbsecret.username}:${self:custom.dbsecret.password}@${self:custom.dbsecret.host}:${self:custom.dbsecret.port}/dfg",
      AUTH0_CLIENT_PUBLIC_KEY: "${file(./auth0_staging_public_key.pem)}",
      PASSWORD_RESET_TIMEOUT: "172800",
      AUTH0_MANAGEMENT_CLIENT_ID: "AlfwkdW1JylTxl4JpNDkJjRLMPMeH8Uf",
      AUTH0_MANAGEMENT_CLIENT_SECRET:
        "4FuIR-LZomy7g_jVNJ26D0V4T2pKslhF2_tAxBDNvWOyaL1XoSLuy2m1I6DCSxxT",
      STRAPI_DOMAIN: "cms.stg.dfg-launch.com",
      STRAPI_USERNAME: "backendlambda@dfg.com",
      STRAPI_PW: "#@+vBSX7VWAc$u@B",
    },
    iam: {
      role: {
        statements: [
          {
            Effect: "Allow",
            Action: ["secretsmanager:*"],
            Resource: "*",
          },
        ],
        managedPolicies: [
          "arn:aws:iam::aws:policy/service-role/AWSLambdaVPCAccessExecutionRole",
        ],
      },
    },
  },
  // import the function via paths
  functions: {
    hello,
    signup,
    auth,
    saveanswer,
    dbmigration,
    getanswers,
    urlLogin,
    login,
    changePassword,
    requestChangePassword,
    changeUsername,
  },
  package: { individually: true },
  custom: {
    esbuild: {
      bundle: true,
      minify: false,
      sourcemap: true,
      exclude: ["aws-sdk", "pg-native", "superagent-proxy"],
      target: "node14",
      define: { "require.resolve": undefined },
      platform: "node",
      concurrency: 10,
    },
    dbsecret:
      "${ssm:/aws/reference/secretsmanager/dfg_staging_connectionstring}",
  },
  resources: {
    Resources: {
      GatewayResponse: {
        Type: "AWS::ApiGateway::GatewayResponse",
        Properties: {
          ResponseParameters: {
            "gatewayresponse.header.Access-Control-Allow-Origin": "'*'",
            "gatewayresponse.header.Access-Control-Allow-Headers": "'*'",
          },
          ResponseType: "EXPIRED_TOKEN",
          RestApiId: { Ref: "ApiGatewayRestApi" },
          StatusCode: "401",
        },
      },
      AuthFailureGatewayResponse: {
        Type: "AWS::ApiGateway::GatewayResponse",
        Properties: {
          ResponseParameters: {
            "gatewayresponse.header.Access-Control-Allow-Origin": "'*'",
            "gatewayresponse.header.Access-Control-Allow-Headers": "'*'",
          },
          ResponseType: "UNAUTHORIZED",
          RestApiId: { Ref: "ApiGatewayRestApi" },
          StatusCode: "401",
        },
      },
    },
  },
};

module.exports = serverlessConfiguration;
