module.exports = {
    extends: [
      'eslint:recommended',
      'plugin:@typescript-eslint/eslint-recommended',
      'plugin:@typescript-eslint/recommended',
      'prettier',
      'plugin:prettier/recommended',
      'plugin:import/recommended'
    ],
    env: {
      node: true,
    },
    parser: '@typescript-eslint/parser',
    plugins: ['@typescript-eslint', 'import'],
    settings: {
      'import/parsers': {
        '@typescript-eslint/parser': ['.ts', '.tsx'],
      },
      'import/resolver': {
        "typescript": {
            "alwaysTryTypes": true, // always try to resolve types under `<root>@types` directory even it doesn't contain any source code, like `@types/unist`
        }
      },
    },
    parserOptions: {
      project: './tsconfig.json',
      tsconfigRootDir: './',
      sourceType: 'module',
      ecmaVersion: 2019,
    },
    rules: {
      '@typescript-eslint/no-explicit-any': 'error',
      '@typescript-eslint/no-unused-vars': 
        [
          "warn",
          { 
            argsIgnorePattern: '^_',
            varsIgnorePattern: '^_',
            caughtErrorsIgnorePattern: '^_',
          },
        ],
      "sort-imports": 
        [
          "error", 
          { 
            "ignoreCase": true, 
            "ignoreDeclarationSort": true 
          }
        ], 
    },
  };