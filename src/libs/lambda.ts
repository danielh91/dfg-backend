import middy from "@middy/core";
import cors, { Options } from "@middy/http-cors";
import middyJsonBodyParser from "@middy/http-json-body-parser";

export const middyfy = (handler) => {
  return middy(handler)
    .use(
      cors({
        getOrigin: (incomingOrigin: string, _: Options) => {
          return incomingOrigin === process.env.FRONTEND_URL
            ? process.env.FRONTEND_URL
            : "http://localhost:3000";
        },
        credentials: true,
        methods: "OPTIONS,POST,GET,PUT,PATCH",
        headers: "Content-Type",
      })
    )
    .use(middyJsonBodyParser());
};
