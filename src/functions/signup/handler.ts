import { ValidatedEventAPIGatewayProxyEvent } from "@libs/api-gateway";

import { middyfy } from "@libs/lambda";
import axios, { AxiosResponse } from "axios";
import IEventResult from "src/types/IEventResultGeneric";
import IEventType from "src/types/IEventTypeGeneric";
import { DbHelper } from "src/utils/DbHelper";
import AES from "crypto-js/aes";
import { ResponseHelper } from "src/utils/ResponseHelper";

interface SignupBody {
  mail: string;
  name: string;
}

const signup: ValidatedEventAPIGatewayProxyEvent<
  IEventType<SignupBody>
> = async (event): Promise<IEventResult<string>> => {
  const pw = randomPassword();
  const requestBody = event.body as SignupBody;

  const magic_data = AES.encrypt(
    JSON.stringify({ email: requestBody.mail, password: pw }),
    process.env.CREDENTIALS_ENCRYPTION_KEY
  ).toString();

  const magic_url = encodeURI(
    `${process.env.FRONTEND_URL}/account/login?data=${magic_data}`
  );

  let response: AxiosResponse;

  try {
    response = await axios.post(
      `https://${process.env.AUTH0_DOMAIN}/dbconnections/signup`,
      {
        client_id: process.env.AUTH0_CLIENT_ID,
        email: requestBody.mail,
        password: pw,
        connection: process.env.AUTH0_REALM,
        name: requestBody.name,
        user_metadata: { pw: pw, magic_url: magic_url },
      }
    );
  } catch (error) {
    return ResponseHelper.createJSONResponse(400, {
      error: "The email address has already been registered",
    });
  }

  console.log("User created in auth0");

  await createUser(
    response.data._id,
    requestBody.name as string,
    requestBody.mail as string
  );

  console.log("User created in backend db");

  return ResponseHelper.createJSONResponseOk({ data: response.data, event });
};

async function createUser(
  id: string,
  name: string,
  email: string
): Promise<void> {
  const client = await DbHelper.getPgClient();

  try {
    await client.connect();
    await client.query(
      "INSERT INTO users (id, name, email) VALUES ($1, $2, $3)",
      [id, name, email]
    );
  } catch (error) {
    console.log(error);
    throw error;
  } finally {
    await client.end();
  }
}

function randomPassword(): string {
  const chars = [
    "abcdefghijklmnopqrstuvwxyz",
    "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
    "0123456789",
    "-!?#",
  ];

  let password = "";

  for (let i = 0; i < 10; i++) {
    const word = chars[i % chars.length];
    password = password + word[Math.floor(Math.random() * word.length)];
  }

  return password;
}

export const main = middyfy(signup);
