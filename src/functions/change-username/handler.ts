import { ValidatedEventAPIGatewayProxyEvent } from "@libs/api-gateway";

import { middyfy } from "@libs/lambda";
import axios, { AxiosResponse } from "axios";
import IEventResult from "src/types/IEventResultGeneric";
import IEventType from "src/types/IEventTypeGeneric";
import { ManagementHelper, ManagementToken } from "src/utils/ManagementHelper";
import { ResponseHelper } from "src/utils/ResponseHelper";
import { UserHelper } from "src/utils/UserHelper";

interface ChangeNameBody {
  email: string;
  newName: string;
}

const changeUsername: ValidatedEventAPIGatewayProxyEvent<
  IEventType<ChangeNameBody>
> = async (event): Promise<IEventResult<string>> => {
  const requestBody = event.body as ChangeNameBody;

  if (!requestBody.newName || !requestBody.newName.length) {
    return ResponseHelper.createJSONResponse(400, {
      error: "New name should not be empty!",
    });
  }

  console.log("Getting Auth0 management api token");
  const token: string = await ManagementHelper.getManagementAccessToken();

  console.log("Search user by mail");
  const userId = await UserHelper.getUserIdByEmail(requestBody.email, token);

  console.log("Send PATCH request to Auth0");
  const response = await axios.request({
    method: "PATCH",
    url: `https://${process.env.AUTH0_DOMAIN}/api/v2/users/${userId}`,
    headers: {
      "content-type": "application/json",
      authorization: `Bearer ${token}`,
    },
    timeout: 2000,
    data: {
      name: requestBody.newName,
      connection: process.env.AUTH0_REALM,
    },
  });

  console.log(response);
};

export const main = middyfy(changeUsername);
