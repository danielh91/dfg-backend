export default {
  type: "object",
  properties: {
    questionId: { type: "integer" },
    answerIds: { type: "array", items: { type: "number" } },
    freeTextAnswer: { type: "string" },
  },
  required: ["questionId"],
} as const;
