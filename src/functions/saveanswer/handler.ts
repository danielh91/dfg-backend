import type { ValidatedEventAPIGatewayProxyEvent } from "@libs/api-gateway";
import { middyfy } from "@libs/lambda";
import { DbHelper } from "src/utils/DbHelper";
import IEventType from "src/types/IEventTypeGeneric";
import IEventResult from "src/types/IEventResultGeneric";
import { UserHelper } from "src/utils/UserHelper";

interface SaveAnswerBody {
  questionId: number;
  answerIds: number[];
  freeTextAnswer: string;
}

const handler: ValidatedEventAPIGatewayProxyEvent<
  IEventType<SaveAnswerBody>
> = async (event): Promise<IEventResult<string>> => {
  const userId = UserHelper.getUserIDFromContext(event.requestContext);

  const requestBody = event.body as SaveAnswerBody;
  const client = await DbHelper.getPgClient();

  const values: string[] = [
    userId,
    requestBody.questionId,
    requestBody.answerIds,
    requestBody.freeTextAnswer,
  ];

  try {
    await client.connect();
    await client.query("BEGIN");
    await client.query(
      "DELETE FROM answers WHERE userid=$1 AND questionid=$2",
      [userId, requestBody.questionId]
    );
    await client.query(
      "INSERT INTO answers (userid, questionid, answerids, freetextanswer) VALUES ($1, $2, $3, $4)",
      values
    );
    await client.query("COMMIT");

    return {
      statusCode: 200,
      headers: {},
      body: "{}",
    };
  } catch (error) {
    await client.query("ROLLBACK");
    console.log(error);
    throw error;
  } finally {
    await client.end();
  }
};

export const main = middyfy(handler);
