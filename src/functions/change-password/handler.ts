import type { ValidatedEventAPIGatewayProxyEvent } from "@libs/api-gateway";
import { middyfy } from "@libs/lambda";
import * as pg from "pg";
import IEventType from "src/types/IEventTypeGeneric";
import { UserHelper } from "src/utils/UserHelper";
import { ManagementHelper } from "src/utils/ManagementHelper";
import { DbHelper } from "src/utils/DbHelper";
import { ResponseHelper } from "src/utils/ResponseHelper";

interface ChangePasswordBody {
  email: string;
  secret: string;
  password: string;
}

interface ChangePasswordRequest {
  date_: Date;
}

const changePassword: ValidatedEventAPIGatewayProxyEvent<
  IEventType<ChangePasswordBody>
> = async (event) => {
  const requestBody = event.body as ChangePasswordBody;

  const databaseClient = await DbHelper.getPgClient();

  databaseClient.connect();

  const cprResult = await getChangePasswordRequest(
    databaseClient,
    requestBody.email,
    requestBody.secret
  );

  if (cprResult.rowCount === 0) {
    return ResponseHelper.createJSONResponse(400, {
      error: "Password change request not exists!",
    });
  }

  const changePasswordRequest = cprResult.rows[0] as ChangePasswordRequest;

  const validUntil = new Date();

  validUntil.setSeconds(
    changePasswordRequest.date_.getSeconds() +
      Number.parseInt(process.env.PASSWORD_RESET_TIMEOUT)
  );

  if (new Date().getSeconds() > validUntil.getSeconds()) {
    await deleteChangePasswordRequest(
      databaseClient,
      requestBody.email,
      requestBody.secret
    );
    return ResponseHelper.createJSONResponse(400, {
      error: "Password change request has expired!",
    });
  }

  const managementToken: string =
    await ManagementHelper.getManagementAccessToken();

  const userId = await UserHelper.getUserIdByEmail(
    requestBody.email,
    managementToken
  );

  await UserHelper.setPassword(userId, requestBody.password, managementToken);

  await deleteChangePasswordRequest(
    databaseClient,
    requestBody.email,
    requestBody.secret
  );

  return ResponseHelper.createJSONResponseOk({});
};

async function getChangePasswordRequest(
  databaseClient: pg.Client,
  email: string,
  secret: string
) {
  const query =
    "SELECT date_ FROM password_change_request WHERE email = $1 AND secret = $2";

  return await databaseClient.query(query, [email, secret]);
}

async function deleteChangePasswordRequest(
  databaseClient: pg.Client,
  email: string,
  secret: string
) {
  const query =
    "DELETE FROM password_change_request WHERE email = $1 AND secret = $2";

  return await databaseClient.query(query, [email, secret]);
}

export const main = middyfy(changePassword);
