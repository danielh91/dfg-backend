import type { ValidatedEventAPIGatewayProxyEvent } from "@libs/api-gateway";
import { middyfy } from "@libs/lambda";
import { AxiosResponse } from "axios";
import IEventResult from "src/types/IEventResultGeneric";
import IEventType from "src/types/IEventTypeGeneric";
import { ResponseHelper } from "src/utils/ResponseHelper";
import { UserHelper } from "src/utils/UserHelper";

interface LoginBody {
  mail: string;
  password: string;
}

const login: ValidatedEventAPIGatewayProxyEvent<IEventType<LoginBody>> = async (
  event
): Promise<IEventResult<string>> => {
  const requestBody = event.body as LoginBody;

  let response: AxiosResponse;
  try {
    response = await UserHelper.login(requestBody.mail, requestBody.password);
  } catch (error) {
    return ResponseHelper.createJSONResponse(400, {
      error: "The email address or password is incorrect",
    });
  }

  return ResponseHelper.createJSONResponseOk(response.data);
};

export const main = middyfy(login);
