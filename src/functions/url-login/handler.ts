import type { ValidatedEventAPIGatewayProxyEvent } from "@libs/api-gateway";
import { middyfy } from "@libs/lambda";
import axios, { AxiosResponse } from "axios";
import CryptoJS from "crypto-js";
import { ManagementHelper } from "src/utils/ManagementHelper";
import { UserHelper } from "src/utils/UserHelper";
import { ResponseHelper } from "src/utils/ResponseHelper";

type TokenResponse = {
  access_token: string;
  refresh_token: string;
  id_token: string;
  scope: string;
  expires_in: number;
  token_type: string;
};

type TokenRequest = {
  realm: string;
  client_id: string;
  scope: string;
  grant_type: string;
  username: string;
  password: string;
};

type StrapiRegisterRequest = {
  username: string;
  email: string;
  password: string;
};

type StrapiLoginRequest = {
  identifier: string;
  password: string;
};

type StrapiLoginResponse = {
  jwt: string;
};

function replaceAll(text: string, find: string, replaceTo: string) {
  const re = new RegExp(find, "g");
  text = text.replace(re, replaceTo);
  return text;
}

const urlLogin: ValidatedEventAPIGatewayProxyEvent<
  Record<string, never>
> = async (event) => {
  if (!event.queryStringParameters || !event.queryStringParameters["data"]) {
    throw new Error("data query parameter is required!");
  }

  const data = replaceAll(event.queryStringParameters["data"], " ", "+");

  const decryptedData = CryptoJS.AES.decrypt(
    data,
    process.env.CREDENTIALS_ENCRYPTION_KEY
  ).toString(CryptoJS.enc.Utf8);

  const dataObject: { email: string; password: string } =
    JSON.parse(decryptedData);

  const response = await axios.post<
    TokenResponse,
    AxiosResponse<TokenResponse>,
    TokenRequest
  >(`https://${process.env.AUTH0_DOMAIN}/oauth/token`, {
    realm: process.env.AUTH0_REALM,
    client_id: process.env.AUTH0_CLIENT_ID,
    scope: "offline_access",
    grant_type: "http://auth0.com/oauth/grant-type/password-realm",
    username: dataObject.email,
    password: dataObject.password,
  });

  await registerUserInStrapi(dataObject);

  const managementToken: string =
    await ManagementHelper.getManagementAccessToken();

  const userId = await UserHelper.getUserIdByEmail(
    dataObject.email,
    managementToken
  );

  await UserHelper.updateUserVerifyEmail(userId, managementToken);

  console.log("Email address verified");

  return ResponseHelper.createJSONResponseOk(response.data);
};

async function registerUserInStrapi(dataObject: {
  email: string;
  password: string;
}) {
  try {
    const loginResult = await axios.post<
      StrapiLoginResponse,
      AxiosResponse<StrapiLoginResponse>,
      StrapiLoginRequest
    >(`https://${process.env.STRAPI_DOMAIN}/auth/local`, {
      identifier: process.env.STRAPI_USERNAME,
      password: process.env.STRAPI_PW,
    });

    await axios.post<unknown, AxiosResponse<unknown>, StrapiRegisterRequest>(
      `https://${process.env.STRAPI_DOMAIN}/auth/local/register`,
      {
        username: dataObject.email,
        email: dataObject.email,
        password: dataObject.password,
      },
      { headers: { Authorization: `Bearer ${loginResult.data.jwt}` } }
    );
  } catch (err) {
    //User already exists
    if (err.response && err.response.status !== 400)
      console.log("statt:" + err.response.status);
  }
}

export const main = middyfy(urlLogin);
