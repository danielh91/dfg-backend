import type { ValidatedEventAPIGatewayProxyEvent } from "@libs/api-gateway";
import { formatJSONResponse } from "@libs/api-gateway";
import { middyfy } from "@libs/lambda";
import * as Sequelize from "sequelize";
import { SequelizeStorage, Umzug } from "umzug"; // eslint-disable-line
import * as pg from "pg";
import schema from "./schema";
import { SecretHelper } from "src/utils/SecretHelper";

const hello: ValidatedEventAPIGatewayProxyEvent<typeof schema> = async (
  event
) => {
  const sequelize = await loadSequelize();
  const umzug = new Umzug({
    migrations: {
      glob: [
        "migrations/*.js",
        { cwd: process.env.LAMBDA_TASK_ROOT /* root folder in aws lambda */ },
      ],
      resolve: ({ name, path, context }) => {
        // adjust the migration parameters Umzug will
        // pass to migration methods, this is done because
        // Sequilize-CLI generates migrations that require
        // two parameters be passed to the up and down methods
        // but by default Umzug will only pass the first
        const migration = require(path || ""); // eslint-disable-line
        return {
          name,
          up: async () => migration.up(context, Sequelize),
          down: async () => migration.down(context, Sequelize),
        };
      },
    },
    context: sequelize.getQueryInterface(),
    storage: new SequelizeStorage({ sequelize }),
    logger: console,
  });

  if (event.body.action === "up") {
    await umzug.up().then(function (migrations) {
      console.log("Umzug Migrations UP:", migrations);
    });
  } else if (event.body.action === "down") {
    await umzug.down().then(function (migrations) {
      console.log("Umzug Migrations down:", migrations);
    });
  } else {
    console.log(`Unknown action:${event.body?.action}`);
  }

  return formatJSONResponse({
    message: "Action was successful",
  });
};

async function loadSequelize() {
  const secretName = "dfg_staging_connectionstring";
  const secretResult = await SecretHelper.getSecret(secretName);
  const secret = JSON.parse(secretResult.SecretString);

  const sequelize = new Sequelize.Sequelize({
    username: secret.username,
    host: secret.host,
    database: "dfg",
    password: secret.password,
    port: secret.port,
    dialect: "postgres",
    dialectModule: pg,
  });
  await sequelize.authenticate();
  return sequelize;
}

export const main = middyfy(hello);
