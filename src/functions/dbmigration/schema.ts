export default {
  type: "object",
  properties: {
    action: { type: "string", enum: ["up", "down"] },
  },
  required: ["action"],
} as const;
