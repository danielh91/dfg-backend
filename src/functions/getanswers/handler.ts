import type { ValidatedEventAPIGatewayProxyEvent } from "@libs/api-gateway";
import { middyfy } from "@libs/lambda";
import { DbHelper } from "src/utils/DbHelper";
import IEventType from "src/types/IEventTypeGeneric";
import IEventResult from "src/types/IEventResultGeneric";

const handler: ValidatedEventAPIGatewayProxyEvent<IEventType<void>> = async (
  event
): Promise<IEventResult<string>> => {
  let userId: string = event.requestContext?.authorizer?.principalId;
  if (!userId)
    throw new Error("This endpoint can't be called without authorization");

  if (userId.includes("|")) userId = userId.split("|")[1];

  const client = await DbHelper.getPgClient();
  await client.connect();
  const result = await client.query(
    'SELECT questionid as "questionId", answerids as "answerIds", freetextanswer as "freeTextAnswer" FROM answers WHERE userid=$1',
    [userId]
  );
  await client.end();
  return {
    statusCode: 200,
    headers: {},
    body: JSON.stringify(result.rows),
  };
};

export const main = middyfy(handler);
