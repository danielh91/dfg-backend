import type { ValidatedEventAPIGatewayProxyEvent } from "@libs/api-gateway";
import { middyfy } from "@libs/lambda";
import IEventType from "src/types/IEventTypeGeneric";
import axios, { AxiosResponse } from "axios";
import * as pg from "pg";
import { UserHelper } from "src/utils/UserHelper";
import { ManagementHelper, ManagementToken } from "src/utils/ManagementHelper";
import { DbHelper } from "src/utils/DbHelper";
import { ResponseHelper } from "src/utils/ResponseHelper";

interface ChangePasswordRequestBody {
  email: string;
}

const requestChangePassword: ValidatedEventAPIGatewayProxyEvent<
  IEventType<ChangePasswordRequestBody>
> = async (event) => {
  const requestBody = event.body as ChangePasswordRequestBody;

  const databaseClient = await DbHelper.getPgClient();

  const managementToken: string =
    await ManagementHelper.getManagementAccessToken();

  const userId = await UserHelper.getUserIdByEmail(
    requestBody.email,
    managementToken
  );
  const emailChangeSecret = `${Math.floor(Math.random() * 50000)}`;

  databaseClient.connect();

  await createChangePasswordRequest(
    databaseClient,
    requestBody.email,
    emailChangeSecret
  );

  console.log("Password change request created in the db");

  const url = `${process.env.FRONTEND_URL}/account/reset-password?secret=${emailChangeSecret}&email=${requestBody.email}`;

  try {
    await UserHelper.updateUserChangePasswordUrl(url, userId, managementToken);
  } catch (error) {
    return ResponseHelper.createJSONResponse(400, {
      error: "Failed to request password change email",
    });
  }

  console.log("Updated user profile with password change url in auth0");

  await requestPasswordChangeInAuth0(requestBody.email);

  console.log("Password change requested in auth0");

  return ResponseHelper.createJSONResponseOk({});
};

async function createChangePasswordRequest(
  databaseClient: pg.Client,
  email: string,
  secret: string
) {
  const query =
    "INSERT INTO password_change_request (email, secret, date_) values ($1, $2, $3)";

  return await databaseClient.query(query, [email, secret, new Date()]);
}

async function requestPasswordChangeInAuth0(email: string) {
  return await axios.request({
    method: "POST",
    url: `https://${process.env.AUTH0_DOMAIN}/dbconnections/change_password`,
    headers: { "content-type": "application/json" },
    timeout: 2000,
    data: {
      client_id: process.env.AUTH0_CLIENT_ID,
      email: email,
      connection: process.env.AUTH0_REALM,
    },
  });
}

export const main = middyfy(requestChangePassword);
