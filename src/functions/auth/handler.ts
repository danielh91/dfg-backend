import { Callback, Context } from "aws-lambda";
import * as jwt from "jsonwebtoken";

interface IAuthEvent {
  authorizationToken: string;
  methodArn: string;
}

// Reusable Authorizer function, set on `authorizer` field in serverless.yml
export const main = (
  event: IAuthEvent,
  _context: Context,
  callback: Callback
) => {
  if (!event.authorizationToken) {
    return callback("Unauthorized");
  }

  const tokenParts = event.authorizationToken.split(" ");
  const tokenValue = tokenParts[1];

  if (!(tokenParts[0].toLowerCase() === "bearer" && tokenValue)) {
    // no auth token!
    return callback("Unauthorized");
  }
  const options = {
    audience: process.env.AUTH0_CLIENT_ID,
  };

  try {
    jwt.verify(
      tokenValue,
      process.env.AUTH0_CLIENT_PUBLIC_KEY,
      options,
      (verifyError, decoded) => {
        if (verifyError) {
          // 401 Unauthorized
          console.log(`Token invalid. ${verifyError}`);
          return callback("Unauthorized");
        }

        return callback(
          null,
          generatePolicy(decoded.sub, "Allow", event.methodArn)
        );
      }
    );
  } catch (err) {
    console.log("catch error. Invalid token", err);
    return callback("Unauthorized");
  }
};

interface IPolicyDocument {
  Version: string;
  Statement: unknown[];
}

// Policy helper function
const generatePolicy = (
  principalId: string | (() => string),
  effect: string,
  methodArn: string
) => {
  const authResponse: {
    principalId: string | (() => string);
    policyDocument: IPolicyDocument;
  } = {
    principalId,
    policyDocument: undefined,
  };
  if (effect && methodArn) {
    const policyDocument: IPolicyDocument = {
      Version: "2012-10-17",
      Statement: [
        {
          Action: "execute-api:Invoke",
          Effect: effect,
          Resource: generateWildcardResource(methodArn),
        },
      ],
    };

    authResponse.policyDocument = policyDocument;
  }

  return authResponse;
};

// This is needed because the authorizer caches the policy and it will work only for the specified resource.
// The method changes the resource to a wildcard one thus it will be valid for all of the endpoints within the same api gateway
// converts 'arn:aws:execute-api:us-east-1:826214726395:kkh50f3pxg/*/GET/answers' to
// 'arn:aws:execute-api:us-east-1:826214726395:kkh50f3pxg/*/*'
//https://aws.amazon.com/premiumsupport/knowledge-center/api-gateway-lambda-authorization-errors
function generateWildcardResource(methodArn: string): string {
  const tmp = methodArn.split(":");
  const apiGatewayArnTmp = tmp[5].split("/");

  // Create wildcard resource
  return (
    tmp[0] +
    ":" +
    tmp[1] +
    ":" +
    tmp[2] +
    ":" +
    tmp[3] +
    ":" +
    tmp[4] +
    ":" +
    apiGatewayArnTmp[0] +
    "/*/*"
  );
}
