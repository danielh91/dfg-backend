import SecretsManager from "aws-sdk/clients/secretsmanager";

export class SecretHelper {
  static getSecret = async (key: string) => {
    const region = "us-east-1";

    // Create a Secrets Manager client
    const secretsManagerClient = new SecretsManager({
      region: region,
    });

    const secretResult = await secretsManagerClient
      .getSecretValue({ SecretId: key })
      .promise();

    return secretResult;
  };
}
