import * as pg from "pg";
import { SecretHelper } from "src/utils/SecretHelper";

export class DbHelper {
  static getPgClient = async (): Promise<pg.Client> => {
    const secretName = "dfg_staging_connectionstring";
    const secretResult = await SecretHelper.getSecret(secretName);
    const secret = JSON.parse(secretResult.SecretString);

    return new pg.Client({
      user: secret.username,
      host: secret.host,
      database: "dfg",
      password: secret.password,
      port: secret.port,
    });
  };
}
