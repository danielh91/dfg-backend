import axios, { AxiosResponse } from "axios";

export type ManagementToken = {
  access_token: string;
  expires_in: number;
  scope: string;
  token_type: string;
};

export class ManagementHelper {
  static currentToken: ManagementToken | undefined = undefined;

  static async getManagementAccessToken(): Promise<string> {
    if (!ManagementHelper.currentToken) {
      ManagementHelper.currentToken = (
        await ManagementHelper.getManagementToken()
      ).data;
    }

    return Promise.resolve(this.currentToken.access_token);
  }

  static async getManagementToken(): Promise<AxiosResponse<ManagementToken>> {
    const parameters = new URLSearchParams();

    parameters.append("grant_type", "client_credentials");
    parameters.append("client_id", process.env.AUTH0_MANAGEMENT_CLIENT_ID);
    parameters.append(
      "client_secret",
      process.env.AUTH0_MANAGEMENT_CLIENT_SECRET
    );
    parameters.append(
      "audience",
      `https://${process.env.AUTH0_DOMAIN}/api/v2/`
    );

    return await axios.post(
      `https://${process.env.AUTH0_DOMAIN}/oauth/token`,
      parameters
    );
  }
}
