export class ResponseHelper {
  public static createJSONResponseOk(body: object) {
    return ResponseHelper.createJSONResponse(200, body);
  }

  public static createJSONResponse(statusCode: number, responseBody: object) {
    return {
      statusCode: statusCode,
      headers: {},
      body: JSON.stringify(responseBody),
    };
  }
}
