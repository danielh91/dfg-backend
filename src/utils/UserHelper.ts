import { APIGatewayEventRequestContextWithAuthorizer } from "aws-lambda";
import axios from "axios";
import * as jwt from "jsonwebtoken";
import IEventType from "src/types/IEventTypeGeneric";

export class UserHelper {
  static async getUserIdByEmail(
    email: string,
    managementToken: string
  ): Promise<string> {
    const url = `https://${process.env.AUTH0_DOMAIN}/api/v2/users-by-email?email=${email}&fields=user_id`;

    const result = await axios.get(url, {
      headers: {
        Authorization: `Bearer ${managementToken}`,
      },
      timeout: 2000,
    });

    return result.data[0].user_id;
  }

  static async updateUserVerifyEmail(userId: string, managementToken: string) {
    const data = {
      email_verified: true,
    };

    return await UserHelper.updateUser(data, userId, managementToken);
  }

  static async updateUserChangePasswordUrl(
    url: string,
    userId: string,
    token: string
  ) {
    const data = {
      user_metadata: {
        change_password_url: url,
      },
    };

    return await UserHelper.updateUser(data, userId, token);
  }

  static async updateUser(
    data: object,
    userId: string,
    managementToken: string
  ) {
    return await axios.patch(
      `https://${process.env.AUTH0_DOMAIN}/api/v2/users/${userId}`,
      data,
      {
        headers: {
          Authorization: `Bearer ${managementToken}`,
        },
        timeout: 2000,
      }
    );
  }

  static async login(email: string, password: string) {
    return await axios.post(`https://${process.env.AUTH0_DOMAIN}/oauth/token`, {
      realm: process.env.AUTH0_REALM,
      client_id: process.env.AUTH0_CLIENT_ID,
      scope: "offline_access",
      grant_type: "http://auth0.com/oauth/grant-type/password-realm",
      username: email,
      password: password,
    });
  }

  static async setPassword(userId: string, password: string, token: string) {
    return await axios.request({
      method: "PATCH",
      url: `https://${process.env.AUTH0_DOMAIN}/api/v2/users/${userId}`,
      headers: {
        "content-type": "application/json",
        authorization: `Bearer ${token}`,
      },
      timeout: 2000,
      data: {
        password: password,
        connection: process.env.AUTH0_REALM,
      },
    });
  }

  static getUserIDFromContext(
    requestContext: APIGatewayEventRequestContextWithAuthorizer<{
      [name: string]: string;
    }>
  ): string {
    let userId = requestContext?.authorizer?.principalId;
    if (!userId)
      throw new Error("This endpoint can't be called without authorization");

    if (userId.includes("|")) userId = userId.split("|")[1];

    return userId;
  }

  static getTokenFromContext<T>(event: IEventType<T>) {
    const tokenParts = event.headers["Authorization"].split(" ");
    const tokenValue = tokenParts[1];

    if (!(tokenParts[0].toLowerCase() === "bearer" && tokenValue)) {
      throw Error("Unauthorized!");
    }
    const options = {
      complete: true,
    };

    return jwt.decode(tokenValue, options) as jwt.JwtPayload;
  }
}
