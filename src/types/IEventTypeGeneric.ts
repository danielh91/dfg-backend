export interface Headers {
  [name: string]: string | undefined;
}

export default interface IEventType<T> {
  headers: Headers;
  body: T;
}
