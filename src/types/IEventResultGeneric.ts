import HttpStatusCode from "./HttpStatusCode";

export default interface IEventResult<T> {
  statusCode: HttpStatusCode;
  headers: { [header: string]: string | number | boolean }; //TODO: find some sophisticated lib for header definitions
  body: T;
}
