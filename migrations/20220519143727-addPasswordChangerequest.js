module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('password_change_request', {
      id : {
        type: Sequelize.DataTypes.BIGINT,
        primaryKey: true,
        unique: true,
        autoIncrement: true
      },
      email: {
        type: Sequelize.DataTypes.STRING,
      },
      secret: Sequelize.DataTypes.STRING,
      date_: Sequelize.DataTypes.DATE
    });
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.dropTable('password_change_request');
  }
};
