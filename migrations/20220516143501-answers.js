'use strict';

module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('answers', {
      id: {
        type: Sequelize.DataTypes.BIGINT,
        primaryKey: true,
        unique: true,
        autoIncrement: true
      },
      userid: {
        type: Sequelize.DataTypes.STRING,
        references: { model: 'users', key: 'id' }
      },
      questionid: {
        type: Sequelize.DataTypes.INTEGER,
        allowNull: false,
      },
      answerids: {
        type: Sequelize.DataTypes.ARRAY(Sequelize.DataTypes.INTEGER),
        allowNull: true,
      },
      freetextanswer: {
        type: Sequelize.DataTypes.STRING(2000),
        allowNull: true,
      }
    });

    await queryInterface.addIndex('answers', ['userid', 'questionid']);
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('answers');
    await queryInterface.removeIndex('answers', ['userid', 'questionid']);
  }
};
