module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('users', {
      id: {
        type: Sequelize.DataTypes.STRING,
        primaryKey: true,
        unique: true,
      },
      email: Sequelize.DataTypes.STRING,
      name: Sequelize.DataTypes.STRING
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('users');
  }
};